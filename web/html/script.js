const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const pub = urlParams.get('pubkey');

function formSubmit() {
    user = document.getElementById("username").value;
    pass = document.getElementById("password").value;

    // If username or password is undefined
    if(!(username) || !(password)) {
        alert("Please enter your username and password.");
    }

    var wgcreds = {username: user, password: pass, publickey: pub};

    login(wgcreds);
}

function login(wgcreds) {
    const authserverurl="http://localhost:8081/api/auth/"; // Change this to the URL of the authentication server. If using the included docker-compose on your local machine, this default should work.
    console.log("Logging in user " + wgcreds.username + " with public key " + wgcreds.publickey);
    $.ajax({
        url: authserverurl,
        type: 'post',
        data: JSON.stringify(wgcreds),
        dataType: 'json',
        headers: {
            "Content-Type": "application/json"
        },
        success: function (data) {
            console.log(data);
            $.ajax({
                url: 'http://localhost:45454/return',
                type: 'post',
                data: JSON.stringify(data),
                dataType: 'json',
                headers: {
                    "Content-Type": "application/json"
                },
                success: function (data) {
                    console.info(data);
                }
            })
        }
    })
}

function vpnEnable(pubkey, duration) {

}