const got = require('got')
const Pool = require('pg').Pool
const pool = new Pool({
    user: 'postgres',
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: 5432
})
const wgAuthProxyAddr = "http://" + process.env.WG_AUTH_PROXY_HOST

const auth = (request, response) => {
    const {publickey, username, password} = request.body

    console.log("Received data: username %s, publickey %s", username, publickey)

    pool.query('SELECT username FROM users WHERE username = $1 AND password = crypt($2, password)', [username, password], (error, results) => {
        if(error) {
            throw error
        }

        // The specific user / password combination was not found. Return an error.
        if (results.rowCount != 1) {
            response.status(401).send({Success: false, Message: "Username or password is incorrect."})
            console.log("Authentication for user %s denied.", username)
            response.send
            return
        }


        // Todo: Make allowedips configurable. Probably an environment variable
        var options = {
            uri:    wgAuthProxyAddr + '/api/auth',
            method: 'POST',
            responseType: "json",
            json: {
                "apikey": process.env.WG_AUTH_PROXY_APIKEY, 
                "publickey": publickey, 
                "allowedips": "10.45.9.2/32", 
                "timeoutsec": 30
            }
        }


        got(wgAuthProxyAddr + '/api/auth', options ).then(proxyresponse => {
            if (proxyresponse.body.status == "OK" && proxyresponse.body.timeoutsec > 0) {
                console.log("Added User %s with publickey %s for %ds.", username, proxyresponse.body.publickey, proxyresponse.body.timeoutsec)
                response.status(200).send({"status": "OK", "timeoutsec": proxyresponse.body.timeoutsec})
                return
            }
        })
    })
}

module.exports= {
    auth
}